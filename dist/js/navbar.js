$( document ).ready(function() {
    //Smooth Scroll
        $('a[href*=\\#]').on('click', function(e){
            e.preventDefault();
            $('html, body').animate({
                scrollTop : $(this.hash).offset().top - 80
            }, 500);
        });

    // Navbar
    // var didScroll;
    // var lastScrollTop = 0;
    // var delta = 5;

    // $(window).scroll(function(event){
    //     didScroll = true;
    // });

    // setInterval(function() {
    //     if (didScroll) {
    //         hasScrolled();
    //         didScroll = false;
    //     }
    // }, 0);

    // function hasScrolled() {
    //     var st = $(this).scrollTop();
    //     if(Math.abs(lastScrollTop - st) <= delta)
    //         return;
    //     if (st > lastScrollTop && st > 0){
    //         $('.nav-bar').removeClass('nav-bar--show');
    //     } else {
    //         if(st + $(window).height() < $(document).height()) {
    //             $('.nav-bar').addClass('nav-bar--show');
    //         }
    //     }
    //     lastScrollTop = st;
    // }
});

//navlink dropdown
// $('.nav-bar__nav-links__dropdown').hover(function() {
//     $('.nav-bar__nav-links__dropdown__wrapper').addClass('_show');
// });

$('.nav-bar__nav-links__dropdown').mouseenter( function () {
    $(this).find('.nav-bar__nav-links__dropdown__wrapper').addClass('_show');
});

$('.nav-bar__nav-links__dropdown').mouseleave(function () {
    $(this).find('.nav-bar__nav-links__dropdown__wrapper').removeClass('_show');
}).mouseleave();

//search
$('#search-btn, .popup-wrapper').click(function() {
    $('.popup-wrapper').toggleClass('_active');
    $('.search-wrapper').toggleClass('_active');
});

//drawer-menu
$('.fa-bars').click(function() {
    $('.drawer-btn .fa-bars').addClass('d-none');
    $('.drawer-btn .fa-times').removeClass('d-none');
    $('.nav-bar').addClass('_expanded');
    $('body').css('overflow', 'hidden');
});
$('.fa-times, .nav-bar__nav-links li a, .search-wrapper-m .search-field button').click(function() {
    $('.drawer-btn .fa-bars').removeClass('d-none');
    $('.drawer-btn .fa-times').addClass('d-none');
    $('.nav-bar').removeClass('_expanded');
    $('body').css('overflow', 'auto');
});